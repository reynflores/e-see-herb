package com.google.sample.cloudvision;

public class ItemsListSingleItem {

    private String imgURL,name,description,uses;

    public ItemsListSingleItem(String imgURL, String name, String description, String uses){
        this.imgURL = imgURL;
        this.name = name;
        this.description = description;
        this.uses = uses;
    }

    public String getImgURL(){
        return imgURL;
    }

    public String getName(){
        return name;
    }

    public String getDescription(){
        return description;
    }

    public String getUses(){
        return uses;
    }
}

