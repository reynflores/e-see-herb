package com.google.sample.cloudvision;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerViewAdapterItems extends RecyclerView.Adapter<RecyclerViewAdapterItems.MyViewHolder>{
    private static ClickListener clickListener;

    public ArrayList<ItemsListSingleItem> myValues;

    public RecyclerViewAdapterItems(ArrayList<ItemsListSingleItem> myValues){
        this.myValues = myValues;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_view_layout, parent, false);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Picasso.get()
                .load(myValues.get(position).getImgURL())
                .resize(500,500)
                .into(holder.itemImage);

    }

    @Override
    public int getItemCount() {
        return myValues.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        private ImageView itemImage;
        public MyViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            itemImage = itemView.findViewById(R.id.item_image);

        }

        @Override
        public void onClick(View v){
            clickListener.onItemClick(getAdapterPosition(),v);
        }

        @Override
        public boolean onLongClick(View v){
            clickListener.onItemLongClick(getAdapterPosition(),v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        RecyclerViewAdapterItems.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }

}

