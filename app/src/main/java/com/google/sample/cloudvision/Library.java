package com.google.sample.cloudvision;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Library extends AppCompatActivity {
    TextView txtview_title,txtview_description,txtview_uses;
    ArrayList<ItemsListSingleItem> libraryValues = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        ImageView button_back = findViewById(R.id.button_back);
        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), HomeScreen.class);
                v.getContext().startActivity(intent);
            }
        });

        txtview_title = findViewById(R.id.txtview_title);
        txtview_description = findViewById(R.id.txtview_description);
        txtview_uses = findViewById(R.id.txtview_uses);

        populateRecyclerview();
        updateRecyclerview(libraryValues);
    }

    public void populateRecyclerview(){
        libraryValues.add(new ItemsListSingleItem(
                "https://images.summitmedia-digital.com/smartpar/images/2016/07/28/0728-hlth-7.jpg",
                "Lagundi (Vitex negundo)",
                "commonly known as the Chinese chaste tree, five-leaved chaste tree, or horseshoe vitex, is a large aromatic shrub with quadrangular, densely whitish, tomentose branchlets. Found in Central Luzon(Region III)",
                "for: cough, asthma, fever decoction (Boil raw fruits or leaves in 2 glasses of water for 15 minutes)"
        ));

        libraryValues.add(new ItemsListSingleItem(
                "http://cuzcoeats.com/wp-content/uploads/2013/03/DSCN1708.jpg",
                "Yerba (Hierba) Buena (Mentha cordifelia)",
                "belongs to the mint family. Yerba buena translates as good herb. The specific plant species regarded as yerba buena varies from region to region, depending on what grows wild in the surrounding landscape, or which species is customarily grown in local gardens. Common in Visayas",
                "for: muscle pain, arthritis, rheumatism, cough, headache • Crush the fresh leaves and squeeze sap. Massage sap on painful parts with eucalyptus"
        ));

        libraryValues.add(new ItemsListSingleItem(
                "https://storage.googleapis.com/drhealthbenefits/2018/04/fca80ea1-sambong-leaves.jpg",
                "Sambong (Blumea balsamifera)",
                "used in traditional herbal medicine for the common cold and as a diuretic.It is also used for infected wounds, respiratory infections, and stomach pains in Thai and Chinese folk medicine. Found in Philippines from Northern Luzon to Palawan and down south in Mindanao.",
                "uses: Anti-edema, Diuretic, Anti- urolithiasis • Boil chopped leaves in a glass of water for 15 minutes until one glassful remains. Divide decoction into 3 parts, drink one part 3 times a day"
        ));

        libraryValues.add(new ItemsListSingleItem(
                "https://images.summitmedia-digital.com/smartpar/images/2016/07/28/0728-hlth-8.jpg",
                "Tsaang Gubat (Carmona retusa)",
                "also known as the Fukien tea tree or Philippine tea tree, is a species of flowering plant in the borage family, Boraginaceae. Common in visayas region, bicol region, ilocos region.",
                "for: Diarrhea - Boil chopped leaves into 2 glasses of water for 15 minutes. Divide decoction into 4 parts. Drink 1 part every 3 hours StomachacheStomachache - Boil chopped leaves in 1 glass of water for 15 minutes. Cool and strain."
        ));

        libraryValues.add(new ItemsListSingleItem(
                "https://images.summitmedia-digital.com/smartpar/images/2016/07/28/0728-hlth-1.jpg",
                "Niyug-Niyogan (Quisqualis indica L.)",
                "also known as the Chinese honeysuckle or Rangoon creeper, is a vine with red flower clusters and is found in Asia. It is found in many other parts of the world either as a cultivated ornamental or run wild",
                "for: Anti-helmintic - The seeds are taken 2 hours after supper. If no worms are expelled, the dose may be repeated after one week. "
        ));

        libraryValues.add(new ItemsListSingleItem(
                "https://images.summitmedia-digital.com/smartpar/images/2016/07/28/0728-hlth-4.jpg",
                "Bayabas/Guava (Psidium guajava L.)",
                "common tropical fruit cultivated in many tropical and subtropical regions. Psidium guajava is a small tree in the myrtle family, native to Mexico, Central America, and northern South America. Available throughout the Philippines",
                "for: washing wounds - May be taken 3-4 times a day As gargle and for toothache - Warm decoction is used for gargle. Freshly pounded leaves are used for toothache. Boil chopped leaves for 15 minutes at low fire. Do not cover and then let it cool and strain"
        ));

        libraryValues.add(new ItemsListSingleItem(
                "https://images.summitmedia-digital.com/smartpar/images/2016/07/28/0728-hlth-3b.jpg",
                "Akapulko(Cassia, alata L.)",
                "an important medicinal tree, as well as an ornamental flowering plant in the subfamily Caesalpinioideae. It also known as emperor's candlesticks, candle bush, candelabra bush, Christmas candles, empress candle plant, ringworm shrub, or candletree. Common in tagalog,ilocos and visayas region.",
                "for: Scabies, Anti-fungal and athlete’s foot, tinea flava, ringworm • Fresh, matured leaves are pounded. Apply soap to the affected area 1-2 times a day"
        ));

        libraryValues.add(new ItemsListSingleItem(
                "https://pitahc.files.wordpress.com/2016/02/pansit-pansitan.png",
                "Ulasimang Bato (Peperonica pellucida)",
                "shallow-rooted herb, usually growing to a height of about 15 to 45 cm. it is characterized by succulent stems, shiny, heart-shaped, fleshy leaves and tiny, dot-like seeds attached to several fruiting spikes. It has a mustard-like odor when crushed",
                "for: lowers uric acid, Gout & Rheumatism • One a half cup leaves are boiled in two glass of water over low fire. Do not cover pot. Divide into 3 parts and drink one part 3 times a day"
        ));

        libraryValues.add(new ItemsListSingleItem(
                "https://mysticalmagicalherbs.files.wordpress.com/2013/07/ampalaya2.jpg",
                "Ampalaya",
                "Momordica charantia is a tropical and subtropical vine of the family Cucurbitaceae, widely grown in Asia, Africa, and the Caribbean for its edible fruit. Its many varieties differ substantially in the shape and bitterness of the fruit. Common in region II and IV",
                "for: Diabetes Mellitus (Mild non-insulin dependent) - Chopped leaves then boil in a glass of water for 15 minutes. Do not cover. Cool and strain. Take 1/3 cup 3 times a day after meals"
        ));
    }

    public void updateRecyclerview(ArrayList<ItemsListSingleItem> values){
        RecyclerViewAdapterItems adapter = new RecyclerViewAdapterItems(values);
        RecyclerView mView =  (RecyclerView) this.findViewById(R.id.recyclerview);
        mView.setHasFixedSize(true);
        mView.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        mView.setLayoutManager(llm);

        adapter.setOnItemClickListener(new RecyclerViewAdapterItems.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                txtview_title.setText(libraryValues.get(position).getName());
                txtview_description.setText(libraryValues.get(position).getDescription());
                txtview_uses.setText(libraryValues.get(position).getUses());
            }
            @Override
            public void onItemLongClick(final int position, View v) {

            }
        });
    }

}
